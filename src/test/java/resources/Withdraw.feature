Feature: Withdraw money from account
  As a use who needs to access my allowance
  I want to withdraw cash from my account
  So that I can buy items

  Acceptance Criteria
  - I will see my available balance
  - I am only allowed to withdraw in $5 denominations
  - I cannot withdraw more than my balance
  - I will receive an alert when I have less than $10 in my account
  - I will be prompted if I try to withdraw an amount below $5
  Background: User is logged in
    Given I am logged in at ATM with pin


  Scenario: Withdraw money from an account with sufficient funds
    Given my balance is "100"
    When I withdraw "30"
    Then I receive "30"
    And receive notification "Your balance is $70"

  Scenario: Withdraw money from an account with insufficient funds
    Given my balance is "20"
    When I withdraw "30"
    Then I receive "0"
    And receive notification "You have insufficient funds"

  Scenario: Withdraw money but not in $5 denomination
    Given my balance is "100"
    When I withdraw "29"
    Then I receive "0"
    And receive notification "Please enter an amount in multiple of $5"



  Scenario Outline: Withdraw money multiple cases
    Given my balance is "<balanceAmount>"
    When I withdraw "<withdrawAmount>"
    Then I receive "<cashAmount>"
    And my remaining balance is shown as "<currentBalance>"
    And receive notification "<alertMessage>"
    Examples:
    |balanceAmount| withdrawAmount|cashAmount|currentBalance|alertMessage|
    |100          |30             |30        |70            |Your balance is $70|
    |20           |30             |0         |20            |You have insufficient funds|
    |100          |29             |0         |100           |Please enter an amount in multiple of $5|
    
    
    
    
    