package stepDefinitions;

import bank.Account;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class WithdrawSteps {

    private Account account;
    private int dispensed;

    @Given("I am logged in at ATM with pin")
    public void i_am_logged_in_at_ATM_with_pin() {

    }

    @Given("^my balance is \"([^\"]*)\"$")
    public void my_balance_is(int amount){
        account = new Account(amount);
    }

    @When("^I withdraw \"([^\"]*)\"$")
    public void i_withdraw(int amount){
        dispensed = account.GetCash(amount);
    }

    @Then("^I receive \"([^\"]*)\"$")
    public void i_receive(int amount){
        Assert.assertEquals(dispensed,amount);
    }

    @Then("^receive notification \"([^\"]*)\"$")
    public void receive_notification(String alert) {
        Assert.assertEquals(account.GetAlert(),alert);
    }

    @Then("^my remaining balance is shown as \"([^\"]*)\"$")
    public void my_remaining_balance_is_shown_as(int amount) {
        Assert.assertEquals(account.GetBalance(),amount);
    }

}
