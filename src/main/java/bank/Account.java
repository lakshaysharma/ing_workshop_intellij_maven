package bank;

public class Account {

    private int balance;
    private char alert;


    public Account(int balance){
        this.balance = balance;
    }

    public int GetCash(int amount){
        if(amount>balance){
            alert = 'A';
            return 0;
        }

        if(!CheckIfValidDenomination(amount)){
            alert = 'B';
            return 0;
        }

        balance = balance- amount;
        alert = 'C';
        return amount;
    }

    public int GetBalance(){
        return balance;
    }

    private boolean CheckIfValidDenomination(int amount){
        return (amount % 5 == 0) ? true : false;
    }

    public String GetAlert(){
        switch(alert){
            case 'A': return "You have insufficient funds";
            case 'B': return "Please enter an amount in multiple of $5";
            case 'C': return "Your balance is $" + balance;
        }
        return null;
    }
}
